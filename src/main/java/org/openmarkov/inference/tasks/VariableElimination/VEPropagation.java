/*
 * Copyright 2015 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NormalizeNullVectorException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.inference.BasicOperations;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.inference.heuristic.HeuristicFactory;
import org.openmarkov.core.inference.tasks.Propagation;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNetOperations;
import org.openmarkov.core.model.network.State;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.NoMixedParents;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.OnlyAtemporalVariables;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.DeltaPotential;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.PotentialRole;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.type.BayesianNetworkType;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.MIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.heuristic.minimalFillIn.MinimalFillIn;
import org.openmarkov.inference.heuristic.simpleElimination.SimpleElimination;
import org.openmarkov.inference.tasks.TaskUtilities;
import org.openmarkov.inference.variableElimination.VariableEliminationCore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Task: propagation
 * This task returns the probability of each chance variable and the utility of each utility node.
 *
 * Input: a symmetric network and a list of variables of interest.
 * Optional input: post-resolution evidence.
 *
 * Output: a table for each utility or chance node
 *
 * @author mluque
 * @author fjdiez
 * @author marias
 * @author jperez-martin
 * @author artasom
 */

public class VEPropagation extends Propagation {

	// Attributes
    private VariableEliminationCore variableEliminationCore = null;

    HashMap<Variable, TablePotential> posteriorValues = new HashMap<>();

    // Constructors
    /**
     * @param network Probabilistic network to be resolved
     * @param preResolutionEvidence Pre-resolution evidence
     * @param postResolutionEvidence Post-resolution evidence
     *
     * @throws NotEvaluableNetworkException
     *             Constructor
     * @throws UnexpectedInferenceException
     * @throws IncompatibleEvidenceException
     */
    public VEPropagation(ProbNet network,
                         List<Variable> variablesOfInterest,
                         EvidenceCase preResolutionEvidence,
                         EvidenceCase postResolutionEvidence,
                         List<Variable> conditioningVariable)
            throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
        super(network);
        pNESupport = probNet.getPNESupport();

        setPreResolutionEvidence(preResolutionEvidence);
        setPostResolutionEvidence(postResolutionEvidence);

        if (conditioningVariable != null) {
            setConditioningVariables(conditioningVariable);
        } else {
            setConditioningVariables(new ArrayList<Variable>());
        }
        setHeuristicFactory(new HeuristicFactory() {
            @Override
            public EliminationHeuristic getHeuristic(ProbNet probNet, List<List<Variable>> variables) {
                return new MinimalFillIn(probNet, variables);
            }
        });

        isTemporal = !probNet.hasConstraint(OnlyAtemporalVariables.class);

        checkNetworkConsistency(probNet);
        checkEvidenceConsistency();
        checkPoliciesConsistency();
        resolveNetwork();
        probNet = TaskUtilities.expandNetwork(probNet, isTemporal);
        probNet = TaskUtilities.extendPreResolutionEvidence(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.addPoliciesImposedByUser(probNet);
        probNet = TaskUtilities.applyTransitionTime(probNet, isTemporal);
        probNet = TaskUtilities.applyDiscounts(probNet, isTemporal);
        //probNet = TaskUtilities.scaleUtilitiesUnicriterion(probNet);
        probNet = TaskUtilities.discretizeNonObservedNumericVariables(probNet,getPreResolutionEvidence());
//        TaskUtilities.addPoliciesFromResolution(probNet);
        probNet = TaskUtilities.extendPostResolutionEvidence(probNet,getPostResolutionEvidence());
        probNet = TaskUtilities.removeSuperValueNodes(probNet,getJoinResolutionEvidence());

        List<Variable> variablesOfInterestBelongToEvidence = new ArrayList<>();
        EvidenceCase evidence = getJoinResolutionEvidence();
        List<Variable> evidenceVariables = getJoinResolutionEvidence().getVariables();

        if (variablesOfInterest != null) {
            for (Variable variableOfInterest : variablesOfInterest) {

                try {
                    if (evidenceVariables.contains(probNet.getVariable(variableOfInterest.getName()))) {
                        variablesOfInterestBelongToEvidence.add(probNet.getVariable(variableOfInterest.getName()));
                    }
                    else {
                        ProbNet preprocessedNetwork = probNet.copy();
                        preprocessedNetwork = pruneNetwork(preprocessedNetwork, variableOfInterest);
                        ProbNet markovNetworkInference = TaskUtilities.projectTablesAndBuildMarkovDecisionNetwork(preprocessedNetwork, evidence);
                        InvokeVariableEliminationCore(markovNetworkInference, evidence, variableOfInterest);
                    }
                } catch (NodeNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        // We have to create a potential for each variable of interest that belongs to the evidence
        TablePotential probPotential = null;
        DeltaPotential deltaPotential;
        for (Variable variable : variablesOfInterestBelongToEvidence) {
            deltaPotential =  new DeltaPotential(Collections.singletonList(variable),
                    PotentialRole.CONDITIONAL_PROBABILITY,new State(evidence.getFinding(variable).getState()));
            try {
                probPotential = deltaPotential.tableProject(new EvidenceCase(), null).get(0);
                probPotential.setPotentialRole(PotentialRole.CONDITIONAL_PROBABILITY);

            } catch (NonProjectablePotentialException | WrongCriterionException e) {
                e.printStackTrace();
            }
            try {
                posteriorValues.put(network.getVariable(variable.getName()),probPotential);
            } catch (NodeNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * @param network
     * @param variablesOfInterest
     * @throws NotEvaluableNetworkException
     * @throws IncompatibleEvidenceException
     * @throws UnexpectedInferenceException
     */
    public VEPropagation(ProbNet network, List<Variable> variablesOfInterest)
            throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
        this(network, variablesOfInterest, null, null, null);
    }

    // Methods
    private void resolveNetwork() {
        List<Node> decisionNodes = probNet.getNodes(NodeType.DECISION);
        // If there are any remaining decision nodes in the network, they do not have imposed policies
        try {
            if (!TaskUtilities.hasOnlyChanceNodes(probNet) && TaskUtilities.hasDecisions(probNet)) {
                VEResolution veResolution = new VEResolution(probNet, getPreResolutionEvidence(), getConditioningVariables());

                for (Variable conditioningVariable : conditioningVariables) {
                    if (probNet.getNode(conditioningVariable).getNodeType() == NodeType.DECISION) {
                        decisionNodes.remove(probNet.getNode(conditioningVariable));
                    }
                }

                for (Node decisionNode : decisionNodes) {
                    Potential policy = veResolution.getOptimalPolicy(probNet.getVariable(decisionNode.getName()));
                    if (policy != null) {
                        decisionNode.setPotential(policy);
                    } else if (!getPreResolutionEvidence().contains(decisionNode.getVariable())){
                        throw new IncompatibleEvidenceException("Incompatible evidence");
                    }
                }
            }
        } catch (NodeNotFoundException | IncompatibleEvidenceException | UnexpectedInferenceException | NotEvaluableNetworkException e) {
            e.printStackTrace();
        }
    }

    private void InvokeVariableEliminationCore(ProbNet network, EvidenceCase evidence, Variable variableOfInterest) throws IncompatibleEvidenceException {
        // Build list of variables to eliminate
        List<Variable> variablesToEliminate = probNet.getChanceAndDecisionVariables();
        variablesToEliminate.remove(variableOfInterest);
        //TODO: eliminate the observable variables (DANs)

        // Create heuristic instance
        EliminationHeuristic heuristic = heuristicFactory(network, new ArrayList<Variable>(),
                evidence.getVariables(), getConditioningVariables(), variablesToEliminate);

        try {
            variableEliminationCore = new VariableEliminationCore(network, heuristic, true);
        } catch (UnexpectedInferenceException e) {
            e.printStackTrace();
        }

        TablePotential posteriorValue = null;
        if (probNet.getNode(variableOfInterest).getNodeType() == NodeType.UTILITY) {
            try {
                posteriorValue = variableEliminationCore.getUtility();
                if(posteriorValue == null){
                    posteriorValue = new TablePotential(variableOfInterest, new ArrayList<Variable>());
                }
            } catch (UnexpectedInferenceException e) {
                e.printStackTrace();
            }
        } else {
            posteriorValue = variableEliminationCore.getProbability();
            try {
                if (posteriorValue != null) {
                    if(posteriorValue.getVariables().get(0) != variableOfInterest){
                        // TODO - Comprobar este código
                        List<Variable> oldOrderVariables = new ArrayList<>();
                        oldOrderVariables.addAll(posteriorValue.getVariables());
                        oldOrderVariables.remove(variableOfInterest);

                        List<Variable> orderedVariables = new ArrayList<>();
                        orderedVariables.add(variableOfInterest);
                        orderedVariables.addAll(oldOrderVariables);

                        posteriorValue = DiscretePotentialOperations.reorder(posteriorValue, orderedVariables);
                    }
                    // TODO - Realizar la normalización condicionada
                    if(getConditioningVariables() == null || getConditioningVariables().isEmpty()){
                        DiscretePotentialOperations.normalize(posteriorValue);
                    }
                }
            } catch (NormalizeNullVectorException e) {
                throw new IncompatibleEvidenceException("Incompatible Evidence");
            }
        }

        posteriorValues.put(variableOfInterest, posteriorValue);
    }

    /**
     * Creates an heuristic associated to <code>network</code>
     *
     * @param markovNetworkInference
     *            <code>MarkovDecisionNetwork</code>
     * @param queryVariables
     *            <code>List<Variable></code>
     * @param evidenceVariables
     *            <code>List<Variable></code>
     * @param conditioningVariables
     *            <code>List<Variable></code>
     * @param variablesToEliminate
     *            <code>List<Variable></code>
     * @return <code>EliminationHeuristic</code>
     */
    protected EliminationHeuristic heuristicFactory(ProbNet markovNetworkInference,
                                                    List<Variable> queryVariables, List<Variable> evidenceVariables,
                                                    List<Variable> conditioningVariables, List<Variable> variablesToEliminate) {
        List<List<Variable>> projectedOrderVariables = BasicOperations.projectPartialOrder(
                this.probNet, queryVariables, evidenceVariables, conditioningVariables, variablesToEliminate);

        return heuristicFactory.getHeuristic(markovNetworkInference, projectedOrderVariables);
    }

    /**
     * @param preprocessedNetwork
     * @param variableOfInterest
     * @return
     * @throws IncompatibleEvidenceException 
     */
    private ProbNet pruneNetwork(ProbNet preprocessedNetwork, Variable variableOfInterest) 
    		throws IncompatibleEvidenceException {
    	//Prune all the nodes except the variable of interest and its ancestors (and the corresponding findings).
    	List<Variable> variablesNotToBePruned = new ArrayList<>();
    	variablesNotToBePruned.add(variableOfInterest);
    	for (Node node : ProbNetOperations.getNodeAncestors(preprocessedNetwork.getNode(variableOfInterest))) {
    		variablesNotToBePruned.add(node.getVariable());
    	}
    	for (Finding finding : getJoinResolutionEvidence().getFindings()) {
    		if (!variablesNotToBePruned.contains(finding.getVariable())) {
    			variablesNotToBePruned.add(finding.getVariable());
    		}
    	}
    	return ProbNetOperations.getPruned(preprocessedNetwork, variablesNotToBePruned, getJoinResolutionEvidence());
    }

    @Override
    public HashMap<Variable, TablePotential> getPosteriorValues()
            throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return posteriorValues;
    }

    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints = new ArrayList<>();
        constraints.add(new NoMixedParents());
        constraints.add(new NoSuperValueNode());
        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes = new ArrayList<>();
        networkTypes.add(BayesianNetworkType.getUniqueInstance());
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        networkTypes.add(MIDType.getUniqueInstance());
        return networkTypes;
    }
}