/*
 * Copyright 2015 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.tasks.VariableElimination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.BasicOperations;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.inference.heuristic.HeuristicFactory;
import org.openmarkov.core.inference.tasks.Resolution;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.constraint.NoMixedParents;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.OnlyAtemporalVariables;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.type.BayesianNetworkType;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.MIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.heuristic.simpleElimination.SimpleElimination;
import org.openmarkov.inference.tasks.TaskUtilities;
import org.openmarkov.inference.variableElimination.VariableEliminationCore;

/**
 * Task: resolution
 *
 * Input: a symmetric network (usually containing decisions and utility nodes)
 * Optional input: pre-resolution evidence (E), imposed policies,
 * and observable variables (O) [for the evaluation of DANs]
 *
 * Output: the global expected utility U(E,O), the probability P(E,O),
 * and the optimal policies (a table for each decision)
 *
 * @author mluque
 * @author fjdiez
 * @author marias
 *
 * @author jperez-martin
 * @author artasom
 */

public class VEResolution extends Resolution {

    private VariableEliminationCore variableEliminationCore = null;

    /**
     * @param network Probabilistic network to be resolved
     * @param preResolutionEvidence Pre-resolution evidence
     *
     * @throws NotEvaluableNetworkException
     *             Constructor
     * @throws UnexpectedInferenceException 
     * @throws IncompatibleEvidenceException 
     */
    public VEResolution(ProbNet network,
                        EvidenceCase preResolutionEvidence,
                        List<Variable> conditionningVariables)
    		throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
        super(network);
        this.pNESupport = this.probNet.getPNESupport();

        setPreResolutionEvidence(preResolutionEvidence);
        setConditioningVariables(conditionningVariables);

        setHeuristicFactory(new HeuristicFactory() {
            @Override
            public EliminationHeuristic getHeuristic(ProbNet probNet, List<List<Variable>> variables) {
                return new SimpleElimination(probNet, variables);
            }
        });

        isTemporal = !probNet.hasConstraint(OnlyAtemporalVariables.class);

        checkNetworkConsistency(probNet);
        checkEvidenceConsistency();
        checkPoliciesConsistency();
        probNet = TaskUtilities.expandNetwork(probNet, isTemporal);
        probNet = TaskUtilities.extendPreResolutionEvidence(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.applyTransitionTime(probNet, isTemporal);
        probNet = TaskUtilities.applyDiscounts(probNet, isTemporal);
        probNet = TaskUtilities.scaleUtilitiesUnicriterion(probNet);
        probNet = TaskUtilities.discretizeNonObservedNumericVariables(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.removeSuperValueNodes(probNet,getPreResolutionEvidence());
        ProbNet markovNetworkInference = TaskUtilities.projectTablesAndBuildMarkovDecisionNetwork(probNet, getPreResolutionEvidence());
        InvokeVariableEliminationCore(markovNetworkInference, getPreResolutionEvidence());
        // TODO: unscaling is needed as the potentials are the original ones.
        //if (scaleUtilities) {
            //probNet = TaskUtilities.unscaleUtilitiesUnicriterion(probNet);
        //}
    }

    private void InvokeVariableEliminationCore(ProbNet network, EvidenceCase evidence) throws NotEvaluableNetworkException, UnexpectedInferenceException {
        // Conditioning variables
        List<Variable> conditioningVariables = getConditioningVariables();

        // Build list of variables to eliminate
        List<Variable> variablesToEliminate = probNet.getChanceAndDecisionVariables();
        //TODO: eliminate the observable variables (DANs)

        // Create heuristic instance
        EliminationHeuristic heuristic = heuristicFactory(network, new ArrayList<Variable>(),
        		evidence.getVariables(),conditioningVariables,variablesToEliminate);

        variableEliminationCore = new VariableEliminationCore(network, heuristic, true);
    }

    /**
     * Creates an heuristic associated to <code>network</code>
     *
     * @param markovNetworkInference <code>MarkovDecisionNetwork</code>
     * @param queryVariables <code>List<Variable></code>
     * @param evidenceVariables <code>List<Variable></code>
     * @param conditioningVariables <code>List<Variable></code>
     * @param variablesToEliminate <code>List<Variable></code>
     * @return <code>EliminationHeuristic</code>
     */
    protected EliminationHeuristic heuristicFactory(ProbNet markovNetworkInference,
                                                    List<Variable> queryVariables, List<Variable> evidenceVariables,
                                                    List<Variable> conditioningVariables, List<Variable> variablesToEliminate) {
        List<List<Variable>> projectedOrderVariables = BasicOperations.projectPartialOrder(
                this.probNet, queryVariables, evidenceVariables, conditioningVariables, variablesToEliminate);

        return heuristicFactory.getHeuristic(markovNetworkInference, projectedOrderVariables);
    }

    @Override
    public TablePotential getProbability() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return variableEliminationCore.getProbability();
    }

    @Override
    public TablePotential getUtility() throws UnexpectedInferenceException {
        return variableEliminationCore.getUtility();
    }

    @Override
    public Potential getOptimalPolicy(Variable decisionVariable) {
        return variableEliminationCore.getOptimalPolicy(decisionVariable);
    }

    @Override
    public HashMap<Variable, Potential> getOptimalPolicies() {
        return (HashMap<Variable, Potential>) variableEliminationCore.getOptimalPolicies();
    }

    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints = new ArrayList<>();
        constraints.add(new NoMixedParents());
        constraints.add(new NoSuperValueNode());
        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes = new ArrayList<>();
        networkTypes.add(BayesianNetworkType.getUniqueInstance());
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        networkTypes.add(MIDType.getUniqueInstance());
        return networkTypes;
    }

}