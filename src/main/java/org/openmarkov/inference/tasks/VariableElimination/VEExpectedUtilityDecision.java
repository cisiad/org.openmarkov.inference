/*
 * Copyright 2015 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.BasicOperations;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.inference.heuristic.HeuristicFactory;
import org.openmarkov.core.inference.tasks.ExpectedUtilityDecision;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNetOperations;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.NoMixedParents;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.OnlyAtemporalVariables;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.type.BayesianNetworkType;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.MIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.heuristic.simpleElimination.SimpleElimination;
import org.openmarkov.inference.tasks.TaskUtilities;
import org.openmarkov.inference.variableElimination.VariableEliminationCore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Task: resolution
 *
 * Input: a symmetric network (usually containing decisions and utility nodes)
 * Optional input: pre-resolution evidence (E), imposed policies,
 * and observable variables (O) [for the evaluation of DANs]
 *
 * Output: the global expected utility U(E,O), the probability P(E,O),
 * and the optimal policies (a table for each decision)
 *
 * @author mluque
 * @author fjdiez
 * @author marias
 *
 * @author jperez-martin
 * @author artasom
 */

public class VEExpectedUtilityDecision extends ExpectedUtilityDecision {

    private VEResolution veResolution;

    private List<Variable> orderedVariables;
    /**
     * @param network Probabilistic network to be resolved
     * @param decision The variable for which the expected utility is required
     *
     * @throws NotEvaluableNetworkException
     *             Constructor
     * @throws UnexpectedInferenceException
     * @throws IncompatibleEvidenceException
     */
    public VEExpectedUtilityDecision(ProbNet network, Variable decision)
    		throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
        super(network);

        List<Variable> informationalPredecesors = ProbNetOperations.getInformationalPredecessors(probNet, decision);
        orderedVariables = new ArrayList<>();
        orderedVariables.addAll(informationalPredecesors);
        orderedVariables.remove(decision);
        orderedVariables.add(0, decision);

        try {
            veResolution = new VEResolution(network, null, informationalPredecesors);
        } catch (UnexpectedInferenceException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TablePotential getExpectedUtility() throws UnexpectedInferenceException {
        return DiscretePotentialOperations.reorder(veResolution.getUtility(), orderedVariables);
    }

    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints = new ArrayList<>();
        constraints.add(new NoMixedParents());
        constraints.add(new NoSuperValueNode());
        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes = new ArrayList<>();
        networkTypes.add(BayesianNetworkType.getUniqueInstance());
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        networkTypes.add(MIDType.getUniqueInstance());
        return networkTypes;
    }

}