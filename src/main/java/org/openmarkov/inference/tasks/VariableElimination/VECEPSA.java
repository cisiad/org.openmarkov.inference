/*
* Copyright 2015 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.tasks.CE_PSA;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.GTablePotential;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.type.NetworkType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author jperez-martin
 */
public class VECEPSA extends CE_PSA {

    private List<GTablePotential> ceaResults;

    private EvidenceCase preResolutionEvidence;

    private Variable decision;

    private int progress;

    /**
     * @param network               a symmetric network having at least two criteria (and usually decisions and utility nodes)
     * @param preResolutionEvidence pre-resolution evidence
     * @throws NotEvaluableNetworkException
     */
    public VECEPSA(ProbNet network, EvidenceCase preResolutionEvidence, Variable decision, int numSimulations, boolean useMultithreading)
    		throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
    	super(network);
    	this.preResolutionEvidence = preResolutionEvidence;
    	this.decision = decision;

    	List<GTablePotential> results = new ArrayList<>();
    	progress = 0;
    	if (useMultithreading) {
    		int numThreads = Runtime.getRuntime().availableProcessors();
    		boolean success = false;
    		while (!success && numThreads > 0) {
    			ExecutorService executor = Executors.newFixedThreadPool(numThreads);
    			List<Future<GTablePotential>> list = new ArrayList<>();
    			for (int i = 0; i < numSimulations; ++i) {
    				Simulation simulation = new Simulation(probNet);
    				list.add(executor.submit(simulation));
    			}
    			int simulationIndex = 0;
    			try {
    				for (Future<GTablePotential> result : list) {
    					results.add(result.get());
    					progress = simulationIndex * 100 / numSimulations;
    					simulationIndex++;
    				}
    				success = true;
    			} catch (InterruptedException | ExecutionException e) {
    				System.out.println("WARNING: PSA failed with " + numThreads + " threads.");
    				e.printStackTrace();
    				System.out.println(e.getMessage());
    				results.clear();
    				numThreads /= 2;
    			}
    		}
    		this.ceaResults = results;
    		progress = 100;
    	} else {
    		for (int i = 0; i < numSimulations; ++i) {
    			sampleNetworkPotentials(probNet);
    			VECEADecision veceaDecision = null;
    			veceaDecision = new VECEADecision(probNet, preResolutionEvidence, decision);
    			results.add(veceaDecision.getCEPPotential());
    			progress = i * 100 / numSimulations;
    		}
    		this.ceaResults = results;
    		progress = 100;
    	}
    }

    public List<GTablePotential> getCeaResults() {
        return this.ceaResults;
    }

    public int getProgress() {
        return progress;
    }

    @Override
    /**
     * No additional constraints
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        return new ArrayList<>();
    }

    @Override
    /**
     * No initialized network types (it calls VECEADecision
     */
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        return new ArrayList<>();
    }

    private class Simulation implements Callable<GTablePotential> {

        ProbNet probNet;

        public Simulation(ProbNet probNet) {
            super();
            this.probNet = probNet;
        }

        @Override
        public GTablePotential call() throws Exception {
            sampleNetworkPotentials(probNet);
            GTablePotential result = null;
            VECEADecision veceaDecision = null;
            try {
                veceaDecision = new VECEADecision(probNet, preResolutionEvidence, decision);
                result = veceaDecision.getCEPPotential();
            } catch (NotEvaluableNetworkException e) {
                e.printStackTrace();
            } catch (IncompatibleEvidenceException e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    private void sampleNetworkPotentials(ProbNet probNet) {
        for (Node node : probNet.getNodes()) {
            List<Potential> sampledPotentials = new ArrayList<>();
            for (Potential potential : node.getPotentials()) {
                sampledPotentials.add(potential.sample());
            }
            node.setPotentials(sampledPotentials);
        }

    }

    @Override
    public Collection<GTablePotential> getCEPPotential() {
        return ceaResults;
    }
}
