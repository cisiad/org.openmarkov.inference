/*
* Copyright 2015 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.BasicOperations;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.inference.heuristic.HeuristicFactory;
import org.openmarkov.core.inference.tasks.CEADecision;
import org.openmarkov.core.model.network.CEP;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.NoMixedParents;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.OnlyAtemporalVariables;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.GTablePotential;
import org.openmarkov.core.model.network.type.BayesianNetworkType;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.MIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.heuristic.minimalFillIn.MinimalFillIn;
import org.openmarkov.inference.heuristic.simpleElimination.SimpleElimination;
import org.openmarkov.inference.tasks.TaskUtilities;
import org.openmarkov.inference.variableElimination.VariableEliminationCore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jperez-martin
 */
public class VECEADecision extends CEADecision {

    private VariableEliminationCore variableEliminationCore = null;

    /**
     * @param network a symmetric network having at least two criteria (and usually decisions and utility nodes)
     * @param preResolutionEvidence pre-resolution evidence
     * @throws NotEvaluableNetworkException
     * @throws IncompatibleEvidenceException 
     * @throws UnexpectedInferenceException 
     */
    public VECEADecision(ProbNet network, EvidenceCase preResolutionEvidence, Variable decision) 
    		throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
        super(network);
        pNESupport = this.probNet.getPNESupport();

        setPreResolutionEvidence(preResolutionEvidence);
        setConditioningVariables(new ArrayList<Variable>());

        setHeuristicFactory(new HeuristicFactory() {
            @Override
            public EliminationHeuristic getHeuristic(ProbNet probNet, List<List<Variable>> variables) {
                return new MinimalFillIn(probNet, variables);
            }
        });

        isTemporal = !probNet.hasConstraint(OnlyAtemporalVariables.class);

		checkNetworkConsistency(probNet);
        checkEvidenceConsistency();

        probNet = TaskUtilities.expandNetwork(probNet,isTemporal);
        probNet = TaskUtilities.scaleUtilitiesCostEffectiveness(probNet);
        probNet = TaskUtilities.extendPreResolutionEvidence(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.applyTransitionTime(probNet, isTemporal);
        probNet = TaskUtilities.applyDiscounts(probNet, isTemporal);
        probNet = TaskUtilities.discretizeNonObservedNumericVariables(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.removeSuperValueNodes(probNet, getPreResolutionEvidence());
        ProbNet markovNetworkInference = null;
		markovNetworkInference = TaskUtilities.projectTablesAndBuildMarkovDecisionNetwork(probNet, getPreResolutionEvidence());
		InvokeVariableEliminationCore(markovNetworkInference, getPreResolutionEvidence(), decision);
    }

    /**
     * @param network
     * @param evidence
     * @param decision
     * @throws UnexpectedInferenceException
     */
    private void InvokeVariableEliminationCore(ProbNet network, EvidenceCase evidence, Variable decision) 
    		throws UnexpectedInferenceException {
        // Conditioning variables
        List<Variable> conditioningVariables = getConditioningVariables();

        // Build list of variables to eliminate
        List<Variable> variablesToEliminate = probNet.getChanceAndDecisionVariables();
        // And remove the received decision from them
        variablesToEliminate.remove(decision);
        //TODO: eliminate the observable variables (DANs)

        // Create heuristic instance
        EliminationHeuristic heuristic = heuristicFactory(network, new ArrayList<Variable>(), evidence.getVariables(),
                conditioningVariables, variablesToEliminate);

        variableEliminationCore = new VariableEliminationCore(network,heuristic, false);
    }

    /**
     * Creates an heuristic associated to <code>network</code>
     *
     * @param markovNetworkInference <code>MarkovDecisionNetwork</code>
     * @param queryVariables <code>List<Variable></code>
     * @param evidenceVariables <code>List<Variable></code>
     * @param conditioningVariables <code>List<Variable></code>
     * @param variablesToEliminate <code>List<Variable></code>
     * @return <code>EliminationHeuristic</code>
     */
    protected EliminationHeuristic heuristicFactory(ProbNet markovNetworkInference,
                                                    List<Variable> queryVariables, List<Variable> evidenceVariables,
                                                    List<Variable> conditioningVariables, List<Variable> variablesToEliminate) {
        List<List<Variable>> projectedOrderVariables = BasicOperations.projectPartialOrder(
                this.probNet, queryVariables, evidenceVariables, conditioningVariables, variablesToEliminate);

        return heuristicFactory.getHeuristic(markovNetworkInference, projectedOrderVariables);
    }

    @Override
    public GTablePotential getCEPPotential() throws UnexpectedInferenceException {
        return (GTablePotential) variableEliminationCore.getUtility();
    }

    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints = new ArrayList<>();
        constraints.add(new NoMixedParents());
        constraints.add(new NoSuperValueNode());
        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes = new ArrayList<>();
        networkTypes.add(BayesianNetworkType.getUniqueInstance());
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        networkTypes.add(MIDType.getUniqueInstance());
        return networkTypes;
    }
}
