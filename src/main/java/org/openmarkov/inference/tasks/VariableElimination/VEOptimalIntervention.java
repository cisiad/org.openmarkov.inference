/*
* Copyright 2015 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.BasicOperations;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.inference.heuristic.HeuristicFactory;
import org.openmarkov.core.inference.tasks.OptimalIntervention;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.NoMixedParents;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.OnlyAtemporalVariables;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.Intervention;
import org.openmarkov.core.model.network.type.BayesianNetworkType;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.MIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.heuristic.simpleElimination.SimpleElimination;
import org.openmarkov.inference.tasks.TaskUtilities;
import org.openmarkov.inference.variableElimination.VariableEliminationCore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jperez-martin
 */
public class VEOptimalIntervention extends OptimalIntervention {

    private VEResolution veResolution = null;

    /**
     * @param probNet a network (usually containing decisions and utility nodes)
     * @throws NotEvaluableNetworkException
     */
    public VEOptimalIntervention(ProbNet probNet) throws NotEvaluableNetworkException, IncompatibleEvidenceException {
        this(probNet, new EvidenceCase());
    }

    /**
     * @param network  a network (usually containing decisions and utility nodes)
     * @param preResolutionEvidence pre-resolution evidence
     * @throws NotEvaluableNetworkException
     */
    public VEOptimalIntervention(ProbNet network, EvidenceCase preResolutionEvidence) throws NotEvaluableNetworkException, IncompatibleEvidenceException {
        super(network);

        try {
            veResolution = new VEResolution(network, preResolutionEvidence, null);
        } catch (UnexpectedInferenceException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Intervention getOptimalIntervention() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return veResolution.getUtility().interventions[0];
    }

    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints = new ArrayList<>();
        constraints.add(new NoMixedParents());
        constraints.add(new NoSuperValueNode());
        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes = new ArrayList<>();
        networkTypes.add(BayesianNetworkType.getUniqueInstance());
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        networkTypes.add(MIDType.getUniqueInstance());
        return networkTypes;
    }
}
