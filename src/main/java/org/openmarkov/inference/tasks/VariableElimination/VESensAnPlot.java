package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.tasks.SensAnPlot;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.modelUncertainty.AxisVariation;
import org.openmarkov.core.model.network.modelUncertainty.SystematicSampling;
import org.openmarkov.core.model.network.modelUncertainty.UncertainParameter;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.type.NetworkType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author jperez-martin
 */
public class VESensAnPlot extends SensAnPlot {

    private HashMap<UncertainParameter, TablePotential> uncertainParametersPotentials;

    /**
     * @param probNet The network used in the inference
     * @throws NotEvaluableNetworkException
     */
    public VESensAnPlot(ProbNet probNet, EvidenceCase preResolutionEvidence, UncertainParameter uncertainParameter,
                        AxisVariation axisVariation, int numberOfIntervals)
            throws NotEvaluableNetworkException {
        this(probNet, preResolutionEvidence, uncertainParameter, axisVariation, numberOfIntervals, null);
    }

    public VESensAnPlot(ProbNet probNet, EvidenceCase preResolutionEvidence, UncertainParameter uncertainParameter,
                        AxisVariation axisVariation, int numberOfIntervals, Variable decisionVariable)
            throws NotEvaluableNetworkException {
        super(probNet);
        uncertainParametersPotentials = new HashMap<>();

        String iterationVariableName = "***Iteration***";
        VEResolution veResolution = null;
        double hMin = axisVariation.getMinValue(uncertainParameter);
        double hMax = axisVariation.getMaxValue(uncertainParameter);
        ProbNet sampledProbNet = SystematicSampling.sampleNetwork(this.probNet, uncertainParameter, hMin, hMax, numberOfIntervals, iterationVariableName);

        List<Variable> variablesConditioning = new ArrayList<>();
        Variable conditionedVariable;
        try {
            conditionedVariable = sampledProbNet.getVariable(iterationVariableName);
            variablesConditioning.add(conditionedVariable);
        } catch (NodeNotFoundException e) {
            e.printStackTrace();
        }

        if (decisionVariable != null) {
            variablesConditioning.add(decisionVariable);
        }

        try {
            veResolution = new VEResolution(sampledProbNet, preResolutionEvidence, variablesConditioning);
        } catch (IncompatibleEvidenceException | UnexpectedInferenceException e) {
            e.printStackTrace();
        }

        // Collect the conditional potential
        TablePotential globalUtility = null;

        try {
            if (veResolution != null) {
                globalUtility = veResolution.getUtility();
            }

            if (decisionVariable != null && globalUtility != null) {
                globalUtility = DiscretePotentialOperations.reorder(globalUtility, variablesConditioning);

            }
        } catch (UnexpectedInferenceException e) {
            e.printStackTrace();
        }

        uncertainParametersPotentials.put(uncertainParameter, globalUtility);

    }

    public HashMap<UncertainParameter, TablePotential> getUncertainParametersPotentials(){
        return uncertainParametersPotentials;
    }

    @Override
    protected List<PNConstraint> initializeAdditionalConstraints() {
        return new ArrayList<>();
    }

    @Override
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        return new ArrayList<>();
    }
}
