package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.tasks.SensAnMap;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.modelUncertainty.AxisVariation;
import org.openmarkov.core.model.network.modelUncertainty.SystematicSampling;
import org.openmarkov.core.model.network.modelUncertainty.UncertainParameter;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.type.NetworkType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author jperez-martin
 */
public class VESensAnMap extends SensAnMap {
    private HashMap<UncertainParameter, TablePotential> uncertainParametersPotentials;

    public VESensAnMap(ProbNet probNet, EvidenceCase preResolutionEvidence,
                       UncertainParameter hUncertainParameter, AxisVariation hAxisVariation,
                       UncertainParameter vUncertainParameter, AxisVariation vAxisVariation,
                       int numberOfIntervals)
            throws NotEvaluableNetworkException {
        this(probNet, preResolutionEvidence, hUncertainParameter, hAxisVariation,
                vUncertainParameter, vAxisVariation, numberOfIntervals, null);
    }

    public VESensAnMap(ProbNet probNet, EvidenceCase preResolutionEvidence,
                       UncertainParameter hUncertainParameter, AxisVariation hAxisVariation,
                       UncertainParameter vUncertainParameter, AxisVariation vAxisVariation,
                       int numberOfIntervals, Variable decisionVariable)
            throws NotEvaluableNetworkException {

        super(probNet);
        uncertainParametersPotentials = new HashMap<>();

        String iterationFirstVariableName = "***Iteration***";
        String iterationSecondVariableName = "***Iteration2***";
        VEResolution veResolution = null;
        double hMin = hAxisVariation.getMinValue(hUncertainParameter);
        double hMax = hAxisVariation.getMaxValue(hUncertainParameter);
        double vMin = vAxisVariation.getMinValue(vUncertainParameter);
        double vMax = vAxisVariation.getMaxValue(vUncertainParameter);

        ProbNet sampledProbNet = SystematicSampling.sampleNetwork(this.probNet, hUncertainParameter, hMin, hMax,
                vUncertainParameter, vMin, vMax, numberOfIntervals, iterationFirstVariableName, iterationSecondVariableName);


        List<Variable> variablesConditioning = new ArrayList<>();
        try {
            variablesConditioning.add(sampledProbNet.getVariable(iterationFirstVariableName));
            variablesConditioning.add(sampledProbNet.getVariable(iterationSecondVariableName));
        } catch (NodeNotFoundException e) {
            e.printStackTrace();
        }

        if(decisionVariable != null) {
            variablesConditioning.add(decisionVariable);
        }

        try {
            veResolution = new VEResolution(sampledProbNet, preResolutionEvidence, variablesConditioning);
        } catch (IncompatibleEvidenceException | UnexpectedInferenceException e) {
            e.printStackTrace();
        }

        // Collect the conditional potential
        TablePotential globalUtility = null;

        try {
            if (veResolution != null) {
                globalUtility = veResolution.getUtility();
            }

            if (decisionVariable != null && globalUtility != null) {
                globalUtility = DiscretePotentialOperations.reorder(globalUtility, variablesConditioning);
            }
        } catch (UnexpectedInferenceException e) {
            e.printStackTrace();
        }

        uncertainParametersPotentials.put(hUncertainParameter, globalUtility);
    }


    public HashMap<UncertainParameter, TablePotential> getUncertainParametersPotentials(){
        return uncertainParametersPotentials;
    }

    @Override
    protected List<PNConstraint> initializeAdditionalConstraints() {
        return new ArrayList<>();
    }

    @Override
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        return new ArrayList<>();
    }
}
