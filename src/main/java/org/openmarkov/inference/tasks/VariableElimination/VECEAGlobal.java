/*
* Copyright 2015 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.BasicOperations;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.inference.heuristic.HeuristicFactory;
import org.openmarkov.core.inference.tasks.CEAGlobal;
import org.openmarkov.core.model.network.*;
import org.openmarkov.core.model.network.constraint.NoMixedParents;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.OnlyAtemporalVariables;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.GTablePotential;
import org.openmarkov.core.model.network.type.BayesianNetworkType;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.MIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.heuristic.minimalFillIn.MinimalFillIn;
import org.openmarkov.inference.heuristic.simpleElimination.SimpleElimination;
import org.openmarkov.inference.tasks.TaskUtilities;
import org.openmarkov.inference.variableElimination.VariableEliminationCore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jperez-martin
 */
public class VECEAGlobal extends CEAGlobal {

    private VariableEliminationCore variableEliminationCore = null;

    /**
     * @param probNet a symmetric network having at least two criteria (and usually decisions and utility nodes)
     * @throws NotEvaluableNetworkException
     */
    public VECEAGlobal(ProbNet probNet) throws NotEvaluableNetworkException, IncompatibleEvidenceException {
        this(probNet, new EvidenceCase());
    }



    /**
     * @param network a symmetric network having at least two criteria (and usually decisions and utility nodes)
     * @param preResolutionEvidence pre-resolution evidence
     * @throws NotEvaluableNetworkException
     */
    public VECEAGlobal(ProbNet network, EvidenceCase preResolutionEvidence) throws NotEvaluableNetworkException, IncompatibleEvidenceException {
        super(network);
        setPreResolutionEvidence(preResolutionEvidence);
        pNESupport = this.probNet.getPNESupport();
        setConditioningVariables(new ArrayList<Variable>());

        setHeuristicFactory(new HeuristicFactory() {
            @Override
                         public EliminationHeuristic getHeuristic(ProbNet probNet, List<List<Variable>> variables) {
               return new MinimalFillIn(probNet, variables);
               // return  new FileElimination(probNet,variables,"C:\\Temp\\variablesToEliminate.txt");
            }
        });

        isTemporal = !probNet.hasConstraint(OnlyAtemporalVariables.class);

        checkNetworkConsistency(probNet);
        checkEvidenceConsistency();

        probNet = TaskUtilities.expandNetwork(probNet,isTemporal);
        probNet = TaskUtilities.extendPreResolutionEvidence(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.addPoliciesImposedByUser(probNet);
        // TODO - Check if necessary
        // probNet = ProbNetOperations.convertNumericalVariablesToFS(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.applyTransitionTime(probNet,isTemporal);
        probNet = TaskUtilities.applyDiscounts(probNet, isTemporal);
        probNet = TaskUtilities.scaleUtilitiesCostEffectiveness(probNet);
        probNet = TaskUtilities.discretizeNonObservedNumericVariables(probNet, getPreResolutionEvidence());
        probNet = TaskUtilities.removeSuperValueNodes(probNet, getPreResolutionEvidence());

        ProbNet markovNetworkInference = TaskUtilities.projectTablesAndBuildMarkovDecisionNetwork(probNet, getPreResolutionEvidence());

//        List<Node> emptyProbNodes = new ArrayList<>();
//
//        for (Node node : probNet.getNodes(NodeType.CHANCE)) {
//            List<Potential> utilityPotentials = new ArrayList<>();
//            List<Potential> probPotentials = new ArrayList<>();
//            for (Potential potential : node.getPotentials()) {
//                if (potential.getPotentialRole() == PotentialRole.UTILITY) {
//                    utilityPotentials.add(potential);
//                } else {
//                    probPotentials.add(potential);
//                }
//            }
//            if (probPotentials != null && probPotentials.isEmpty()) {
//                emptyProbNodes.add(node);
//            }
//        }

        InvokeVariableEliminationCore(markovNetworkInference, getPreResolutionEvidence());

    }


    private void InvokeVariableEliminationCore(ProbNet network, EvidenceCase evidence) {
        // Conditioning variables
        List<Variable> conditioningVariables = getConditioningVariables();

        // Build list of variables to eliminate
        List<Variable> variablesToEliminate = probNet.getChanceAndDecisionVariables();
        //TODO: eliminate the observable variables (DANs)

        // Create heuristic instance
        EliminationHeuristic heuristic = heuristicFactory(network, new ArrayList<Variable>(), evidence.getVariables(),
                conditioningVariables, variablesToEliminate);

        try {
            variableEliminationCore = new VariableEliminationCore(network,heuristic, false);
        } catch (UnexpectedInferenceException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an heuristic associated to <code>network</code>
     *
     * @param markovNetworkInference <code>MarkovDecisionNetwork</code>
     * @param queryVariables <code>List<Variable></code>
     * @param evidenceVariables <code>List<Variable></code>
     * @param conditioningVariables <code>List<Variable></code>
     * @param variablesToEliminate <code>List<Variable></code>
     * @return <code>EliminationHeuristic</code>
     */
    protected EliminationHeuristic heuristicFactory(ProbNet markovNetworkInference,
                                                    List<Variable> queryVariables, List<Variable> evidenceVariables,
                                                    List<Variable> conditioningVariables, List<Variable> variablesToEliminate) {
        List<List<Variable>> projectedOrderVariables = BasicOperations.projectPartialOrder(
                this.probNet, queryVariables, evidenceVariables, conditioningVariables, variablesToEliminate);

        return heuristicFactory.getHeuristic(markovNetworkInference, projectedOrderVariables);
    }

    @Override
    public CEP getCEP() throws UnexpectedInferenceException {
        return (CEP) ((GTablePotential) variableEliminationCore.getUtility()).elementTable.get(0);
    }

    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints = new ArrayList<>();
        constraints.add(new NoMixedParents());
        constraints.add(new NoSuperValueNode());
        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected  List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes = new ArrayList<>();
        networkTypes.add(BayesianNetworkType.getUniqueInstance());
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        networkTypes.add(MIDType.getUniqueInstance());
        return networkTypes;
    }

}
