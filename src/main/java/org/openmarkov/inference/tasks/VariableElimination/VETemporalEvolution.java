package org.openmarkov.inference.tasks.VariableElimination;

import org.openmarkov.core.exception.*;
import org.openmarkov.core.inference.tasks.TemporalEvolution;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.constraint.NoMixedParents;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.type.BayesianNetworkType;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.MIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.tasks.TaskUtilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author jperez-martin
 */
public class VETemporalEvolution extends TemporalEvolution {


    private HashMap<Variable, TablePotential> posteriorValues;
    private EvidenceCase networkEvidence;


    /**
     * @param network The network used in the inference
     * @throws NotEvaluableNetworkException
     */
    public VETemporalEvolution(ProbNet network, Variable temporalVariable, EvidenceCase preResolutionEvidence,
                               Variable decisionVariable)
            throws NotEvaluableNetworkException {
        super(network);

        this.networkEvidence = new EvidenceCase(preResolutionEvidence);
        this.pNESupport = this.probNet.getPNESupport();

        this.probNet = TaskUtilities.expandNetwork(this.probNet,true);

        List<Variable> variablesOfInterest = new ArrayList<>();
        for(int i = 0; i <= probNet.getInferenceOptions().getTemporalOptions().getNumberOfSlices(); i++){
            Variable variableInSlice = null;
            try {
                // if the variable exists in this slice add to variables of intereset
                variableInSlice = this.probNet.getVariable(temporalVariable.getBaseName(), i);
                if(variableInSlice != null) {
                    variablesOfInterest.add(variableInSlice);
                }
            } catch (NodeNotFoundException e) {
//                e.printStackTrace();
            }

        }

        checkNetworkConsistency(probNet);
        checkEvidenceConsistency();
        checkPoliciesConsistency();

        try {
            VEPropagation vePosteriorValues = null;
            if (decisionVariable != null) {
                vePosteriorValues = new VEPropagation(this.probNet, variablesOfInterest, this.networkEvidence, new EvidenceCase(), Collections.singletonList(decisionVariable));
            }
            else {
                vePosteriorValues = new VEPropagation(this.probNet, variablesOfInterest, this.networkEvidence, new EvidenceCase(), null);
            }
            posteriorValues = vePosteriorValues.getPosteriorValues();
        } catch (IncompatibleEvidenceException | UnexpectedInferenceException e) {
            e.printStackTrace();
        }
    }


    @Override
    public HashMap<Variable, TablePotential> getPosteriorValues() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return this.posteriorValues;
    }
    
    public ProbNet getExpandedNetwork(){
        return this.probNet;
    }


    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    protected List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints = new ArrayList<>();
        constraints.add(new NoMixedParents());
        constraints.add(new NoSuperValueNode());
        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes = new ArrayList<>();
        networkTypes.add(BayesianNetworkType.getUniqueInstance());
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        networkTypes.add(MIDType.getUniqueInstance());
        return networkTypes;
    }
}
