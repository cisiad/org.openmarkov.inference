package org.openmarkov.inference.tasks;

import org.junit.Test;
import org.openmarkov.core.exception.ConstraintViolationException;
import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.ParserException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.InferencePropagationTaskIDTest;
import org.openmarkov.core.inference.tasks.Propagation;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.PotentialRole;
import org.openmarkov.core.model.network.potential.UniformPotential;
import org.openmarkov.inference.tasks.VariableElimination.VEPropagation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VEPropagationTest extends InferencePropagationTaskIDTest {
	@Override
	public Propagation buildInferenceTask(ProbNet probNet, List<Variable> variablesOfInterest,
								   EvidenceCase preResolutionEvidence, EvidenceCase postResolutionEvidence)
			throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
		return new VEPropagation(probNet, variablesOfInterest, preResolutionEvidence, postResolutionEvidence,null);
	}

	/**
	 * Test for diagnosis problem
	 * @throws ParserException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws NodeNotFoundException
	 * @throws ConstraintViolationException
	 * @throws NotEvaluableNetworkException
	 */
	@Test
	public void testImposePoliciesIDDiagnosisProblem()
			throws FileNotFoundException,
			IOException, ParserException, NodeNotFoundException,
			ConstraintViolationException, NotEvaluableNetworkException {
/*		ProbNet network = IDFactory.buildIDPerfectKnowledge();
		addUniformPolicy(network,"Therapy");
		InferenceAlgorithm algorithm = buildInferenceAlgorithm(network);

		try {
			// test max expected utility
			Double meuEvaluation = algorithm.getGlobalUtility().values[0];
			assertEquals(8.94, meuEvaluation, maxError);
		} catch (Exception e) {
			printExceptionAndFailIfImplemented(e);
		}*/
	}


	private void addUniformPolicy(ProbNet network, String nameDecision) throws NodeNotFoundException {
		List<Variable> variables = new ArrayList<>();
		Variable variableD = network.getVariable(nameDecision);
		variables.add(variableD);
		Potential potentialD = new UniformPotential(variables,PotentialRole.POLICY);
		network.addPotential(potentialD);
	}



}