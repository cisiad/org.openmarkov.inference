package org.openmarkov.inference.tasks;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.OptimalStrategyTaskIDTest;
import org.openmarkov.core.inference.tasks.OptimalIntervention;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.inference.tasks.VariableElimination.VEOptimalIntervention;

public class VEOptimalStrategyTest extends OptimalStrategyTaskIDTest {
	@Override
	public OptimalIntervention buildInferenceTask(ProbNet probNet, EvidenceCase preResolutionEvidence)
			throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
		return new VEOptimalIntervention(probNet, preResolutionEvidence);
	}
}