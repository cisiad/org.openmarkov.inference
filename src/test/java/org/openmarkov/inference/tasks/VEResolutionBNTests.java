/*
 * Copyright 2015 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 *//*


package org.openmarkov.inference.tasks;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.InferenceResolutionTaskBNTest;
import org.openmarkov.core.inference.tasks.Resolution;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.inference.tasks.VariableElimination.VEResolution;

*/
/**
 * Unit test for VEResolution.
 *//*

public class VEResolutionBNTests extends InferenceResolutionTaskBNTest {

    @Override
    public Resolution buildInferenceTask(ProbNet probNet) throws NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
        return new VEResolution(probNet, new EvidenceCase());
    }
}
*/
