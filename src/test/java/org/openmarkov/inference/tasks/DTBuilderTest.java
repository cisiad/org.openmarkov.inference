package org.openmarkov.inference.tasks;

import static org.junit.Assert.assertNotNull;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.ParserException;
import org.openmarkov.core.io.ProbNetInfo;
import org.openmarkov.core.model.decisiontree.DecisionTreeElement;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.inference.tasks.decisiontree.DTBuilder;
import org.openmarkov.io.probmodel.PGMXReader;

/**
 * Test of the task that creates a decision tree.
 *  
 * @author Manuel Arias
 */
public class DTBuilderTest {

	// Attributes
	private DTBuilder builder;
	
	private final String netwoksPath = "";
	
	private final String ID_CEA_minimal = "ID-CEA-minimal.pgmx";
	
	@Test
	public void testBuildDecisionTree() {
		// Read network and ensure it has been readed
		ProbNet probNet = readProbNet(netwoksPath + ID_CEA_minimal);
		assertNotNull(probNet);

		// Real test begins: 1) Create tree
		builder = new DTBuilder(probNet);
		assertNotNull(builder);
		
		// 2) Ensure tree is OK.
		DecisionTreeElement dtRoot = builder.getDecisionTree();
		assertNotNull(dtRoot);
	}

	/**
	 * Reads a probNet from resources
	 * @param probNetName
	 * @return A probNet
	 * @throws Exception
	 */
	private ProbNet readProbNet(String probNetName) {
        InputStream file = getClass().getClassLoader().getResourceAsStream(probNetName);

        // Load the network
        PGMXReader pgmxReader = new PGMXReader();
        ProbNetInfo probNetInfo = null;
        try {
            probNetInfo = pgmxReader.loadProbNet(file, probNetName);
        } catch (ParserException e) {
			System.err.println("Error reading network: " + probNetName);
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
        }

		return probNetInfo == null ? null : probNetInfo.getProbNet();
	}

}
